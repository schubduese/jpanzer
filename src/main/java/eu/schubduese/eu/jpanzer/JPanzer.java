package eu.schubduese.eu.jpanzer;

import eu.schubduese.eu.jpanzer.connection.Led;
import eu.schubduese.eu.jpanzer.connection.Movement;

public class JPanzer
{

	public JPanzer()
	{
		start();
	}
	
	private void start()
	{
		Led led = new Led();
		led.setOk();
		Movement movement = new Movement();
		movement.turn90left();
		movement.turn90left2();
		movement.turn90left3();
	}

	public static void main(String[] args)
	{
		new JPanzer();
	}

}
