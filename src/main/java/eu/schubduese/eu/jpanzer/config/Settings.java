package eu.schubduese.eu.jpanzer.config;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.Properties;

import org.apache.log4j.Logger;

import eu.schubduese.eu.jpanzer.utils.StringUtils;

public class Settings
{

	private static Logger logger = Logger.getLogger(Settings.class);
	static Properties 	properties;
	private static File propertiesFile;
	
	public static String loadValue(String cnfKey, String defaultValue)
	{
		String s = loadValue(cnfKey);
		if (StringUtils.isEmpty(s))
			return defaultValue;
		else
			return s;
	}
	
	public static String loadValue(String cnfKey)
	{
		return getProperties().getProperty(cnfKey);
	}
	
	public static int loadIntValue(String cnfKey, int defaultValue)
	{
		int i = loadIntValue(cnfKey);
		if (i == -1)
			return defaultValue;
		else
			return i;
	}
	
	public static boolean loadBoolValue(String cnfKey, boolean defaultValue)
	{
		int i = loadIntValue(cnfKey);
		if (i == -1)
			return defaultValue;
		else 
			return i != 0;
	}
	
	public static boolean loadBoolValue(String cnfKey)
	{
		int i = loadIntValue(cnfKey);
		return i == 1;
	}
	
	public static int loadIntValue(String cnfKey)
	{
		String value = getProperties().getProperty(cnfKey);
		if (StringUtils.isNotEmpty(value))
		{
			try
			{
				return Integer.parseInt(value);
			}
			catch (NumberFormatException e) 
			{
				//hier nichts machen
			}
		}
		return -1;
	}
	
	public static void setBoolValue(String cnfKey, boolean value)
	{
		setValue(cnfKey, value ? "1": "0");
	}

	public static void setValue(String cnfKey, String value)
	{
		if (cnfKey != null && StringUtils.isNotEmpty(value))
			getProperties().setProperty(cnfKey, value);
	}

	private static Properties getProperties()
	{
		if (properties == null)
		{
			try
			{
				FileReader reader = new FileReader(getPropertiesFile());
				properties = new Properties();
				properties.load(reader);
				reader.close();
			} catch (FileNotFoundException ex)
			{
				logger.error(ex);
			} catch (IOException ex)
			{
				logger.error(ex);
			}

		}
		return properties;
	}

	public static boolean save()
	{
		try
		{
			OutputStream outputStream = new FileOutputStream(getPropertiesFile());
			Writer writer = new OutputStreamWriter(outputStream, Charset.forName("UTF-8"));
			getProperties().store(writer, "");
			writer.close();
			return true;
		} catch (FileNotFoundException ex)
		{
			logger.error(ex);
		} catch (IOException ex)
		{
			logger.error(ex);
		}
		return false;
	}
	
	private static File getPropertiesFile()
	{
		if (propertiesFile == null)
		{
			propertiesFile = new File(System.getProperty("user.home") + "/" + ".jpanzer");
			if (!propertiesFile.exists())
			{
				try
				{
					propertiesFile.createNewFile();
				} catch (IOException e)
				{
					logger.error(e);
				}
			}
		}
		return propertiesFile;
	}

}
