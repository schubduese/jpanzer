package eu.schubduese.eu.jpanzer.connection;

import org.apache.log4j.Logger;

public class Led
{

	private Logger logger = Logger.getLogger(Led.class);
	private PanzerConnector panzerConnector;
	
	public Led()
	{
		this.panzerConnector = PanzerConnector.getInstance();
	}
	
	public void setLED(int r, int g, int b)
	{
		panzerConnector.sendMessage("wsR " + r);
		panzerConnector.sendMessage("wsG " + g);
		panzerConnector.sendMessage("wsB " + b);
	}
	
	public void setError()
	{
		setLED(255, 0, 0);
	}
	
	public void setOk()
	{
		setLED(0, 255, 0);
	}
	
}
