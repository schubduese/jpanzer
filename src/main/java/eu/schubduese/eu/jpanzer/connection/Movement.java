package eu.schubduese.eu.jpanzer.connection;

import org.apache.log4j.Logger;

public class Movement
{

	private Logger logger = Logger.getLogger(Movement.class);
	private PanzerConnector panzerConnector;

	public Movement()
	{
		panzerConnector = PanzerConnector.getInstance();
	}

	public void forward()
	{
		panzerConnector.sendMessage("forward");
	}

	public void backward()
	{
		panzerConnector.sendMessage("backward");
	}

	public void left()
	{
		panzerConnector.sendMessage("left");
	}

	public void right()
	{
		panzerConnector.sendMessage("right");
	}
	
	public void stop()
	{
		panzerConnector.sendMessage("DS");
	}
	
	public void turnStop()
	{
		panzerConnector.sendMessage("TS");
	}
	
	public void turn90left()
	{
		panzerConnector.sendMessage("left");
		try
		{
			Thread.sleep(500);
		} catch (InterruptedException e)
		{
			logger.error(e);
		}
		turnStop();
	}
	
	public void turn90left2()
	{
		panzerConnector.sendMessage("left");
		try
		{
			Thread.sleep(1000);
		} catch (InterruptedException e)
		{
			logger.error(e);
		}
		turnStop();
	}
	
	public void turn90left3()
	{
		panzerConnector.sendMessage("left");
		try
		{
			Thread.sleep(800);
		} catch (InterruptedException e)
		{
			logger.error(e);
		}
		turnStop();
	}
	
	public void moveForwardUntilObstacle()
	{
		if (UltrasonicConnector.getUltrasonicValue() < 0.20)
		{
			logger.debug(String.format("Hindernis ist %s entfernt, fahre lieber nicht los", UltrasonicConnector.getUltrasonicValue()));
			return;
		}
		forward();	
		while (UltrasonicConnector.getUltrasonicValue() > 0.20)
		{
			try
			{
				Thread.sleep(100);
			} catch (InterruptedException e)
			{
				logger.error(e);
			}
		}
		stop();
	}
	
}
