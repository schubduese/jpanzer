package eu.schubduese.eu.jpanzer.connection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import org.apache.log4j.Logger;

import eu.schubduese.eu.jpanzer.config.Settings;

public class PanzerConnector
{
	
	private Logger logger = Logger.getLogger(PanzerConnector.class);
	private static PanzerConnector panzerConnector;
	private Socket clientSocket;
	private PrintWriter printWriter;
	private BufferedReader bufferedReader;
	private String host;
	private int port;
	
	private PanzerConnector()
	{
		this(Settings.loadValue("HOST"), Settings.loadIntValue("PORT", 10223));
	}
	
	public PanzerConnector(String host, int port) 
	{
		this.host = host;
		this.port = port;
	}
	
	public static PanzerConnector getInstance()
	{
		if (panzerConnector == null)
			panzerConnector = new PanzerConnector();
		return panzerConnector;
	}
	
	private void connect()
	{
		try
		{
			UltrasonicConnector.getInstance();
			InfoConnector.getInstance();
			if (logger.isTraceEnabled())
				logger.trace(String.format("Stelle Verbindung zu Host %s auf Port %S her", host, port));
			clientSocket = new Socket(host, port);
			printWriter = new PrintWriter(clientSocket.getOutputStream(), true);
			bufferedReader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			if (logger.isInfoEnabled())
				logger.info("Verbindung hergestellt.");
		} catch (UnknownHostException e)
		{
			logger.error(e);
		} catch (IOException e)
		{
			logger.error(e);
		}
	}
	
	public synchronized void sendMessage(String msg)
	{
		if (!isConnected())
			connect();
		if (printWriter == null)
			logger.warn(String.format("%s kann nicht versendet werden da der Socket nicht geöffnet ist", msg));
		else
			printWriter.println(msg);
	}
	
	public boolean isConnected()
	{
		return (clientSocket != null && clientSocket.isConnected());
	}

	public void stopConnection()
	{
		try
		{
			bufferedReader.close();
			printWriter.close();
			clientSocket.close();
			if (logger.isInfoEnabled())
				logger.info("Verbindung geschlossen.");
		} catch (IOException e)
		{
			logger.error(e);
		}
	}

}
