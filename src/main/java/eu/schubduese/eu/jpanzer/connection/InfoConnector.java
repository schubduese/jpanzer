package eu.schubduese.eu.jpanzer.connection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

import org.apache.log4j.Logger;

public class InfoConnector
{

	private Logger logger = Logger.getLogger(InfoConnector.class);
	private ServerSocket ultrasonicSocket;
	private static double ultrasonicValue;
	private boolean isStopped;
	private static InfoConnector ultrasonicConnector;
	private UltraSonicThread ultraSonicThread;

	private InfoConnector()
	{
		ultrasonicValue = 0;
		ultraSonicThread = new UltraSonicThread();
		ultraSonicThread.start();
	}

	public static InfoConnector getInstance()
	{
		if (ultrasonicConnector == null)
			ultrasonicConnector = new InfoConnector();
		return ultrasonicConnector;
	}

	private void startSocket()
	{
		try
		{
			if (logger.isTraceEnabled())
				logger.trace("Starte Socket für Ultraschallwerte auf Port 2256");
			ultrasonicSocket = new ServerSocket(2256);
			isStopped = false;
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		while (!isStopped)
		{
			try
			{
				Socket connectionSocket = ultrasonicSocket.accept();
				BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
				String rawValue = inFromClient.readLine();
				ultrasonicValue = Double.parseDouble(rawValue);
				if (logger.isTraceEnabled())
					logger.trace("Neuen Entfernungswert erhalten: " + rawValue);
			} catch (IOException e)
			{
				logger.error(e);
			}
		}
	}
	
	public void stop()
	{
		try
		{
			isStopped = true;
			ultrasonicSocket.close();
		} catch (IOException e)
		{
			logger.error(e);
		}
	}
	
	private class UltraSonicThread extends Thread
	{
		
		public void run()
		{
			startSocket();
		}
	}

	public static double getUltrasonicValue()
	{
		return ultrasonicValue;
	}
	
	

}
