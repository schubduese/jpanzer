package eu.schubduese.eu.jpanzer.connection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

import org.apache.log4j.Logger;

public class UltrasonicConnector
{

	private Logger logger = Logger.getLogger(UltrasonicConnector.class);
	private ServerSocket ultrasonicSocket;
	private static double ultrasonicValue;
	private boolean isStopped;
	private static UltrasonicConnector ultrasonicConnector;
	private UltraSonicThread ultraSonicThread;

	private UltrasonicConnector()
	{
		ultrasonicValue = 0;
		ultraSonicThread = new UltraSonicThread();
		ultraSonicThread.start();
	}

	public static UltrasonicConnector getInstance()
	{
		if (ultrasonicConnector == null)
			ultrasonicConnector = new UltrasonicConnector();
		return ultrasonicConnector;
	}

	private void startSocket()
	{
		try
		{
			if (logger.isTraceEnabled())
				logger.trace("Starte Socket für Ultraschallwerte auf Port 2257");
			ultrasonicSocket = new ServerSocket(2257);
			isStopped = false;
			PanzerConnector.getInstance().sendMessage("steady");
		} catch (IOException e)
		{
			logger.error(e);
		}
		while (!isStopped)
		{
			try
			{
				Socket connectionSocket = ultrasonicSocket.accept();
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
				String rawValue = bufferedReader.readLine();
				ultrasonicValue = Double.parseDouble(rawValue);
				if (logger.isTraceEnabled())
					logger.trace("Neuen Entfernungswert erhalten: " + rawValue);
			} catch (IOException e)
			{
				logger.error(e);
			}
		}
	}
	
	public void stop()
	{
		try
		{
			isStopped = true;
			ultrasonicSocket.close();
		} catch (IOException e)
		{
			logger.error(e);
		}
	}
	
	private class UltraSonicThread extends Thread
	{
		
		public void run()
		{
			startSocket();
		}
	}

	public static double getUltrasonicValue()
	{
		return ultrasonicValue;
	}
	
	

}
