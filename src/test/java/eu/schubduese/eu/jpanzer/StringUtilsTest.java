package eu.schubduese.eu.jpanzer;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import eu.schubduese.eu.jpanzer.utils.StringUtils;

public class StringUtilsTest 
{
	
    @Test
    public void testEmptyString()
    {
    	assertTrue(StringUtils.isEmpty(""));
    }
    
    @Test
    public void testString()
    {
    	assertFalse(StringUtils.isEmpty("notempty"));
    }
    
    @Test
    public void testNullString()
    {
    	assertTrue(StringUtils.isEmpty(null));
    }
    
    @Test
    public void testNotNullString()
    {
    	assertFalse(StringUtils.isNotEmpty(null));
    }
    
}
